

var music = $('audio')[0]
var moffon = false;
music.onplaying = function () {
  moffon = true;
  $('.musicBtn').css('animation-play-state', 'running');
}
$('.musicBtn').click(function () {
  if (moffon) {
    music.pause();
    $('.musicBtn').css('animation-play-state', 'paused');
    moffon = false;
  } else {
    music.play();
    $('.musicBtn').css('animation-play-state', 'running');
    moffon = true;
  }

});
document.addEventListener("WeixinJSBridgeReady", function () { 
  music.play(); 
  $('.musicBtn').css('animation-play-state', 'running');
  // document.getElementById('video').play(); 
}, false);
var app = {
  start: function () {
    $('.sBtn').click(function () {
      $('.mark').show();
      $('input').focus();
    });
    $('.djks').click(function () {
      var name = $('input').val();
      if (name == '' || !name) {
        layer.msg('填写一个昵称哦！', { time: 1000 });
        return;
      }
      window.sessionStorage.setItem('name', name);
      location.href = './answer.html';
    });
  },
  answer2: function () {
    $('.answerBox ul').on('click', 'li', function () {
      $(this).addClass('active').siblings().removeClass('active');
    });
    var n = 0;
    $('.nexBtn').click(function () {
      n++;
      console.log(data[n]);
      if ($('.answerBox ul li.active').length <= 0) {
        // alert('请选择答案哦！');
        layer.msg('请选择答案哦！', { time: 1000 });
        return;
      }
      if (n > 9) {
        console.log('最后一题');
        location.href = './result.html';
      } else {
        $('.answerBox .answerA').hide();
        $('.answerBox ul').hide();
        $('.answerBox .answerA').text(data[n].a);
        for (var i = 0; i < data[n].q.length; i++) {
          $('.answerBox ul li').eq(i).find('span').text(data[n].q[i]);
        }
        if (n >= 9) {
          $(this).addClass('active');
        }
        $('.answerBox ul li').removeClass('active');
        $('.answerBox .answerA').show();
        $('.answerBox ul').show();
      }

    });
  },
  result: function () {
    var name = window.sessionStorage.getItem('name');
    console.log(name);
    if (!name) {
      location.href = './index.html';
    }
    $('.reBox h3 span').text(name);
    $('.reBox').mousedown(function (e) {
      e.stopPropagation()
    });
    $('.again').click(function () {
      location.href = 'index.html';
    });
    var ca = '';
    // html2canvas($(".bg")[0], {
    //   onrendered: function (canvas) {
    //     //把截取到的图片替换到a标签的路径下载 
    //     ca = canvas.toDataURL();
    //     $(".save a").attr('href', canvas.toDataURL());
    //     //下载下来的图片名字 
    //     $(".save a").attr('download', 'share.png');
    //     //document.body.appendChild(canvas);
    //   }
    //   //可以带上宽高截取你所需要的部分内容 
    //   //     , 
    //   //     width: 300, 
    //   //     height: 300 
    // });
    $('.result .save').click(function () {
      $('.mark').show();
      //Window.href=ca;
    });
    // $('.lingqu').click(function () {
    //   location.href='https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIwNTI1MjQ5Mw==&scene=124#wechat_redirect';
    // })
    $('.result .mark').click(function () {
      $('.mark').hide();
    });
    $('.result .mark div').click(function (event) {
      event.stopPropagation()
    });
    var index = Math.floor((Math.random() * hudata.length));
    console.log(hudata[index]);
    // console.log(hudata);
    $('.result .imgBox img').attr('src', 'images/' + (index + 1) + hudata[index].t + '.png');
    $('.result .huname div').text(hudata[index].t);
    $('.result .content div').text(hudata[index].r);
  },
  answer: function () {
    var mySwiper = new Swiper('.swiper-container', {
      direction: 'vertical',
      lazyLoading: true,
      shortSwipes: true,
      mousewheelControl: true,
      watchSlidesProgress: true,
      onInit: function (swiper) {
        swiper.myactive = 0;
      },
      onProgress: function (swiper) {
        for (var i = 0; i < swiper.slides.length; i++) {
          var slide = swiper.slides[i];
          var progress = slide.progress;
          var translate, boxShadow;

          translate = progress * swiper.height * 0.8;
          scale = 1 - Math.min(Math.abs(progress * 0.2), 1);
          boxShadowOpacity = 0;

          slide.style.boxShadow = '0px 0px 10px rgba(0,0,0,' + boxShadowOpacity + ')';

          if (i == swiper.myactive) {
            es = slide.style;
            es.webkitTransform = es.MsTransform = es.msTransform = es.MozTransform = es.OTransform = es.transform = 'translate3d(0,' + (translate) + 'px,0) scale(' + scale + ')';
            es.zIndex = 0;


          } else {
            es = slide.style;
            es.webkitTransform = es.MsTransform = es.msTransform = es.MozTransform = es.OTransform = es.transform = '';
            es.zIndex = 1;

          }

        }

      },


      onTransitionEnd: function (swiper, speed) {
        for (var i = 0; i < swiper.slides.length; i++) {
          //	es = swiper.slides[i].style;
          //	es.webkitTransform = es.MsTransform = es.msTransform = es.MozTransform = es.OTransform = es.transform = '';

          //	swiper.slides[i].style.zIndex = Math.abs(swiper.slides[i].progress);


        }

        swiper.myactive = swiper.activeIndex;

      },
      onSetTransition: function (swiper, speed) {

        for (var i = 0; i < swiper.slides.length; i++) {
          //if (i == swiper.myactive) {

          es = swiper.slides[i].style;
          es.webkitTransitionDuration = es.MsTransitionDuration = es.msTransitionDuration = es.MozTransitionDuration = es.OTransitionDuration = es.transitionDuration = speed + 'ms';
          //}
        }

      }
    })
  }
}